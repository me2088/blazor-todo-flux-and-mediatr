using Microsoft.EntityFrameworkCore;
using TodoApplication.Server.Models;

namespace TodoApplication.Server.Data;

public class TodoContext: DbContext
{
    public DbSet<TodoItem> TodoItems { get; set; } = null!;
    
    public TodoContext(DbContextOptions<TodoContext> options) : base(options)
    {
    } 
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
    }
}