using AutoMapper;
using TodoApplication.Server.Models;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Server.Profiles;

public class TodoItemProfile: Profile
{
   public TodoItemProfile()
   {
      this.CreateMap<TodoItem, TodoItemDto>();
      this.CreateMap<TodoItemDto, TodoItem>();
   } 
}