using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApplication.Server.Models;

[Table("TodoItems")]
public class TodoItem
{
    [Key]
    public int TodoItemId { get; set; }

    public string Title { get; set; } = null!;
}