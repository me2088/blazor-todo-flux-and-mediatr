using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TodoApplication.Server.Data;
using TodoApplication.Shared.Dtos;
using TodoApplication.Shared.Queries;

namespace TodoApplication.Server.Handlers;

public class GetTodoItemsHandler : IRequestHandler<GetTodoItemsQuery, List<TodoItemDto>>
{
    private readonly TodoContext _context;
    private readonly IMapper _mapper;

    public GetTodoItemsHandler(IMapper mapper, TodoContext context)
    {
        _mapper = mapper;
        _context = context;
    }

    public async Task<List<TodoItemDto>> Handle(GetTodoItemsQuery request, CancellationToken cancellationToken)
    {
        var todoItems = await _context.TodoItems
            //.Where(t => t.UserId == request.UserId)
            .ToListAsync(cancellationToken);

        // Convert the TodoItem entities to DTOs
        return _mapper.Map<List<TodoItemDto>>(todoItems);
    }
}
