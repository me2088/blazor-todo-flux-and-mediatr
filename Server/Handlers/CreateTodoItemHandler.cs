using AutoMapper;
using MediatR;
using TodoApplication.Server.Data;
using TodoApplication.Server.Models;
using TodoApplication.Shared.Commands;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Server.Handlers;


public class CreateTodoItemHandler : IRequestHandler<CreateTodoItemCommand, TodoItemDto>
{
    private readonly TodoContext _context;
    private readonly IMapper _mapper;

    public CreateTodoItemHandler(IMapper mapper, TodoContext context)
    {
        _mapper = mapper;
        _context = context;
    }

    public async Task<TodoItemDto> Handle(CreateTodoItemCommand request, CancellationToken cancellationToken)
    {
        var todoItem = new TodoItem
        {
            Title = request.Title,
            // Initialize other fields as necessary.
        };

        _context.TodoItems.Add(todoItem);
        await _context.SaveChangesAsync(cancellationToken);

        // Convert the entity to a DTO before returning.
        return _mapper.Map<TodoItemDto>(todoItem);
    }
}
 