using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TodoApplication.Shared.Commands;
using TodoApplication.Shared.Dtos;
using TodoApplication.Shared.Queries;

namespace TodoApplication.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemController : ControllerBase
    {
        private readonly IMediator _mediator;
        public TodoItemController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<TodoItemDto>>> CreateTodoItem(GetTodoItemsQuery query)
        {
            return await _mediator.Send(query);
        }
        
        
        [HttpPost]
        public async Task<ActionResult<TodoItemDto>> CreateTodoItem(CreateTodoItemCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}
