using MediatR;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Shared.Commands;

public class CreateTodoItemCommand: IRequest<TodoItemDto>
{
   public string Title { get; set; } = null!; 
}