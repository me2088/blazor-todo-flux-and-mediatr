using MediatR;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Shared.Queries;

public class GetTodoItemsQuery: IRequest<List<TodoItemDto>>
{
   /// <summary>
   /// User id to query on, if not specified returns all
   /// </summary>
   /// <returns></returns>
   public string? UserId
   {
      get; set;
   } 
   
   public GetTodoItemsQuery(string? userId)
   {
      UserId = userId;
   }
}