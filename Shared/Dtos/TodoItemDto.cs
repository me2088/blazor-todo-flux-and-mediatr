namespace TodoApplication.Shared.Dtos;

public class TodoItemDto
{
    public int TodoItemId { get; set; }
    public string Title { get; set; }
}