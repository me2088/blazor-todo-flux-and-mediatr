using TodoApplication.Shared.Dtos;

namespace TodoApplication.Client.Actions;

public record AddTaskResultAction(string ColumnName, TodoItemDto Task);
public record AddTaskAction(string ColumnName, TodoItemDto Task);
