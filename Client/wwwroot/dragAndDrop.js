window.dragAndDrop = {
    init: function (dropContainer) {
        dropContainer.addEventListener('dragover', function (event) {
            event.preventDefault();
        });

        dropContainer.addEventListener('drop', function (event) {
            event.preventDefault();
        });
    },

    performDrop: function (dropContainer, itemId) {
        // Find the dragged item based on the itemId
        var draggedItem = document.querySelector(`[data-item-id="${itemId}"]`);

        // Append the dragged item to the drop container
        dropContainer.appendChild(draggedItem);
    }
};
