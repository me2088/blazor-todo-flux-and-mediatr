using Fluxor;
using TodoApplication.Client.Actions;
using TodoApplication.Client.Services;

namespace TodoApplication.Client.Effects;

public class AddTaskEffect: Effect<AddTaskAction>
{
    private readonly ITaskService _taskService;

    public AddTaskEffect(ITaskService taskService)
    {
        _taskService = taskService;
    }
    
    public override async Task HandleAsync(AddTaskAction action, IDispatcher dispatcher)
    {
        Console.WriteLine($"Start Handling AddTaskEffect");
        var task = await _taskService.AddTask(action.ColumnName, action.Task);
        Console.WriteLine($"Got new Todo Item from server: {task}");
        dispatcher.Dispatch(new AddTaskResultAction(action.ColumnName, task));
        Console.WriteLine($"End Handling AddTaskEffect");
    }
}