using Fluxor;
using TodoApplication.Client.State;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Client.Features;

public class KanbanFeature: Feature<KanbanState>
{
    public override string GetName()
    {
        return "Kanban";
    }

    protected override KanbanState GetInitialState()
    {
        return new KanbanState(new Dictionary<string, List<TodoItemDto>>
        {
            { "Todo", new List<TodoItemDto>(){ 
                new TodoItemDto() { TodoItemId = 1, Title = "First item" }, 
                new TodoItemDto() { TodoItemId = 2, Title = "Second item" } 
            }
            },
            { "Doing", new List<TodoItemDto>() },
            { "Done", new List<TodoItemDto>() }
        });
    }
}