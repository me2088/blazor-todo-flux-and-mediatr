using TodoApplication.Shared.Dtos;

namespace TodoApplication.Client.State;

public record KanbanState(Dictionary<string, List<TodoItemDto>> Columns);

