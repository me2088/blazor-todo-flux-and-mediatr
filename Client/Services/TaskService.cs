using System.Net.Http.Json;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Client.Services;
public interface ITaskService
{
    Task<TodoItemDto> AddTask(string columnName, TodoItemDto task);
}

public class TaskService : ITaskService
{
    // HttpClient is used to make HTTP requests
    private readonly HttpClient _httpClient;

    public TaskService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<TodoItemDto> AddTask(string columnName, TodoItemDto task)
    {
        // You would implement your logic here to make a request to your server API
        // For example:
        var response = await _httpClient.PostAsJsonAsync($"api/TodoItem", task);
        response.EnsureSuccessStatusCode();
        return await response.Content.ReadFromJsonAsync<TodoItemDto>();
    }
}
