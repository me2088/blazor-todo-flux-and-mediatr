using Fluxor;
using TodoApplication.Client.Actions;
using TodoApplication.Client.State;
using TodoApplication.Shared.Dtos;

namespace TodoApplication.Client.Reducers;


public class Reducers
{
    [ReducerMethod]
    public static KanbanState ReduceAddTaskResultAction(KanbanState state, AddTaskResultAction action)
    {
        Console.WriteLine($"Start ReduceAddTaskResultAction: {action.ColumnName}");
        var newColumns = new Dictionary<string, List<TodoItemDto>>(state.Columns);
    
        if (!newColumns.ContainsKey(action.ColumnName))
        {
            newColumns[action.ColumnName] = new List<TodoItemDto>();
        }

        newColumns[action.ColumnName].Add(action.Task);

        Console.WriteLine($"End ReduceAddTaskResultAction: {action.ColumnName}");
        return new KanbanState(newColumns);
    }

}
